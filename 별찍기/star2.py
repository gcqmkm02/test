#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

_GNU = "🐃" # U+1F403
_LINUX = "🐧" # U+1F427

가 = 0 # 초기화
나 = 0 # 초기화

입력 = int(sys.argv[1])

for 단계 in range(입력, 0, -1):
    나 = 나 + 1
    가 = 입력 - 나
    print(가 *  _GNU, 나 * _LINUX, sep = "")
    
# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 6월 18일
