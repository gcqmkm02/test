#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

GNU = "🐃" # U+1F403
LINUX = "🐧" # U+1F427

가 = 0 # 초기화
나 = 0 # 초기화

입력 = int(sys.argv[1])

for 단계 in range(0, 입력):
    가 = 가 + 1
    나 = 입력 - 가
    print(가 * GNU, 나 * LINUX, sep = "")
    
# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 6월 17일
