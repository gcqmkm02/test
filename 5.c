/* -*- coding: utf-8 -*- */ 
 
#include <stdio.h>
#include <stdlib.h>
 
int prime(int n)
{
	int i;
	char F[8] = {'\'', 'F', 'a', 'l', 's', 'e', '\'', '\0',};
	char T[7] = {'\'', 'T', 'r', 'u', 'e', '\'', '\0',};
 
	for(i = 2; i < n; i++) {
		if(n % i == 0) {
			printf("(%d, %s)\n", n, F); 
			return 0;
		}
	}
	printf("(%d, %s)\n", n, T);
	return 0;
}
 
int main(int argc, char **argv)
{
	int i;
	int j = atoi(argv[1]);
 
	for(i = 2;  i < j+1; i++) {
		prime(i);
	}
 
	return 0;
}
 
/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막갱신: 2020년 4월 3일
 */
 
/* EOF */
