/* -*- mode: c -*- */ 
/* -*- coding: utf-8 -*- */ 

#include <stdio.h>
#include <stdlib.h>

/* 
 * 이것은 제가 짠 최초의 C언어 코드입니다
 * 들여쓰기는 리눅스 스타일입니다
 * 주석도 리눅스 스타일입니다
 */

/* argv 로 인수 1개를 입력받아서 나머지값을 출력합니다 */

int main(int argc, char* argv[])
{
	int i = 3;
	int j = atoi(argv[1]);
		
	printf("%d\n", j % i);

	return 0;
}

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막갱신: 2020년 4월 2일
 */

/* EOF */
