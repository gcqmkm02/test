#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# KLDP [163020]

"""
요구조건: 
1. use a string.splitlines() function
2. use a list.split("|") function
3. use a tuple data type
4. use a dict (key type is str, value type is tuple)
5. use a for i in range(len(list)) for dict assigning
6. use a for key, value in dict.items() loop
7. use a print with an f-string format and print with string multiply
    ("=" * 80, "#" * 80)
"""

data_raw = """\
1|天地玄黃|천지현황|天(하늘 천)地(땅 지)玄(검을 현)黃(누를 황)|하늘은 위에 있어 그 빛이 검고 땅은 아래 있어서 그 빛이 누르다.
2|宇宙洪荒|우주홍황|宇(집 우)宙(집 주)洪(넓을 홍)荒(거칠 황)|하늘과 땅 사이는 넓고 커서 끝이 없다. 즉 세상의 넓음을 말한다.
3|日月盈昃|일월영측|日(날 일)月(달 월)盈(찰 영)昃(기울 측)|해는 서쪽으로 기울고 달도 차면 점차 이지러진다. 즉 우주의 진리를 말한다.
4|辰宿列張|진숙열장|辰(별 진)宿(잘 숙)列(벌일 열)張(베풀 장)|성좌가 해 달과 같이 하늘에 넓게 벌려져 있음을 말한다.
5|寒來暑往|한래서왕|寒(찰 한)來(올 래)暑(더울 서)往(갈 왕)|찬 것이 오면 더운 것이 가고 더운 것이 오면 찬 것이 간다. 즉 사철의 바뀜을 말한다.
6|秋收冬藏|추수동장|秋(가을 추)收(거둘 수)冬(겨울 동)藏(감출 장)|가을에 곡식을 거두고 겨울이 오면 그것을 감춰 들인다.
7|閏餘成歲|윤여성세|閏(윤달 윤)餘(남을 여)成(이룰 성)歲(해 세)|일년 이십사절기 나머지 시각을 모아 윤달로 하여 해를 이루었다.
8|律呂調陽|율려조양|律(가락 률)呂(음률 려)調(고를 조)陽(볕 양)|천지간의 양기를 고르게 하니 즉 율은 양이요 여는 음이다.
9|雲騰致雨|운등치우|雲(구름 운)騰(오를 등)致(이를 치)雨(비 우)|수증기가 올라가서 구름이 되고 냉기를 만나 비가 된다. 즉 자연의 기상을 말한다.
10|露結爲霜|노결위상|露(이슬 로)結(맺을 결)爲(할 위)霜(서리 상)|이슬이 맺어 서리가 되니 밤기운이 풀잎에 물방울처럼 이슬을 이룬다.
11|金生麗水|금생여수|金(쇠 금)生(낳을 생)麗(고울 려)水(물 수)|금은 여수에서 나니 여수는 중국의 지명이다.
12|玉出崑岡|옥출곤강|玉(구슬 옥)出(날 출)崑(메 곤)岡(메 강)|옥은 곤강에서 나니 곤강은 역시 중국의 산 이름이다.
13|劍號巨闕|검호거궐|劍(칼 검)號(이름 호)巨(클 거)闕(대궐 궐)|거궐은 칼이름이고 구야자가 지은 보검이다. 즉 조나라의 국보다.
14|珠稱夜光|주칭야광|珠(구슬 주)稱(일컬을 칭)夜(밤 야)光(빛 광)|구슬의 빛이 밤의 낮 같은 고로 야광이라 칭하였다.
15|果珍李柰|과진이내|果(과실 과)珍(보배 진)李(오얏 리)柰(능금나무 내)|과실 중에 오얏과 능금나무의 그 진미가 으뜸임을 말한다.
16|菜重芥薑|채중개강|菜(나물 채)重(무거울 중)芥(겨자 개)薑(생강 강)|나물은 겨자와 생강이 중하다.
17|海鹹河淡|해함하담|海(바다 해)鹹(짤 함)河(물 하)淡(묽을 담)|바다 물은 짜고 밀물은 맛도 없고 맑다.
18|鱗潛羽翔|인잠우상|鱗(비늘 린)潛(잠길 잠)羽(깃 우)翔(높이 날 상)|비늘 있는 고기는 물 속에 잠기고 날개 있는 새는 공중에 난다.
19|龍師火帝|용사화제|龍(용 룡)師(스승 사)火(불 화)帝(임금 제)|복희씨는 용으로써 벼슬을 기록하고 신농씨는 불로써 기록하였다.
20|鳥官人皇|조관인황|鳥(새 조)官(벼슬 관)人(사람 인)皇(임금 황)|소호는 새로써 벼슬을 기록하고 황제는 인문을 갖추었으므로 인황이라 하였다.
"""

data_tuples = [tuple(line.split("|")) for line in data_raw.strip().splitlines(False)]

천자문 = [dict(갑 = x[0], 을 = x[1], 병 = x[2], 정 = x[3], 무 = x[4]) for x in data_tuples]

print(type(천자문)) # <class 'list'>
print(type(천자문[0])) # <class 'dict'>
print(len(천자문)) # 20

# EOF
