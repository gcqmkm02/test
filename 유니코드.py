#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 리눅스/오픈소스 상징물 유니코드 표현

# TUX: https://www.fileformat.info/info/unicode/char/1f427/index.htm
# GNU: https://www.fileformat.info/info/unicode/char/1f403/index.htm
# PYT: https://www.fileformat.info/info/unicode/char/1f40d/index.htm

tux = "🐧"
gnu = "🐃"
pyt = "🐍"

v1 = tux.encode("unicode_escape")
v2 = gnu.encode("unicode_escape")
v3 = pyt.encode("unicode_escape")

print(v1, tux)
print(v2, gnu)
print(v3, pyt)

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 10월 11일

"""
(bionic)soyeomul@localhost:~/test$ ./유니코드.py
b'\\U0001f427' 🐧
b'\\U0001f403' 🐃
b'\\U0001f40d' 🐍
"""
