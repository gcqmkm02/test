#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

# https://kldp.org/node/161823

import subprocess
import re

class mc:
    UIDs = 10 # URL 주소의 범위를 지정하는 값: 유효범위 ===> [1-142]
    BASE_URL = "https://qqalba.com/bbs/board.php?bo_table=job&wr_id="
    WRITE_MODE = False # 파일에 쓰느냐 그냥 print() 하느냐의 선택
    
def make_url(id):
    """
    URL 전체 범위: id=[1-142]
    일단 10 페이지만 실험 ===> id=[1-10]
    """
    id = list(range(1, mc.UIDs+1))
    FURL = []
    for k in id:
        FURL.append("{0}{1}"\
                   .format(mc.BASE_URL, str(k)))

    return FURL

FURL = make_url(id) # FURL 은 반드시 리스트 형식이어야함.

class ac:
    def slide(l, n):
        """
        산법(알고리즘) 출처: (익명 사용자님)
        ===> https://kldp.org/comment/631312#comment-631312
        """
        ret = tuple()
        for e in l:
            ret = ret + (e,)
            if len(ret) == n:
                yield ret
                ret = ret[1:]
            
def get_grep(url): # grep 내부 동작 방식
    p = subprocess.Popen("curl -s '{}'".format(url), \
                         stdout=subprocess.PIPE, text=True, shell=True)
    output = p.stdout.readlines()

    exp = re.compile("id=(.+)$")
    chop = exp.search(url).group(1)
    chop_zfill = chop.zfill(3)
    """
    실제로 찍어보니 없는 URL도 있음.
    그래서 디버깅 차원에서 URL ID 를 목록에 함께 출력되도록 코드를 보완.
    """
    ids = "[uid:{}] ".format(chop_zfill) # 실존하는 URL 의 ID
    
    t_list = []
    s_list = []
    for k, v in ac.slide(output, 2):
        if "연락처" in k:
            tel = v
            t_list.append(tel.strip())
        if "업소명" in k:
            shop = v
            s_list.append(shop.strip())

    if mc.WRITE_MODE == True:        
        for v1, v2 in zip(t_list, s_list):
            with open("161823.txt", "a") as f:
                f.write("{0}{1}{2}{3}\n".format(ids, v1[4:-5], '\t', v2[16:-5]))
    else:
        for v1, v2 in zip(t_list, s_list):
            print("{0}{1}{2}{3}".format(ids, v1[4:-5], '\t', v2[16:-5]))

# url 을 받아서 grep 수행후 결과 출력.
for url in FURL:
    get_grep(url)

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 5일

"""
실험 환경: 우분투 18.04/ 파이썬 3.7/ curl

결과:
(bionic)soyeomul@localhost:~/test$ ./161823.py
[uid:001] 010-8827-7794	가락동 ART2
[uid:002] 010-7654-2119	퀸
[uid:003] 010-2491-2124	안전
[uid:004] 010-4866-9542	Goose Bar
[uid:005] 010-4276-7902	로렌아로마
[uid:006] 010-4044-3727	벤츠
[uid:007] 010-4646-6938	동탄 ♡굿ㆍ향기♡
[uid:008] 010-2123-1522	둘리
[uid:009] 010-4184-5588	BAR11CLUB
[uid:010] 010-4926-1374	꿀다방
(bionic)soyeomul@localhost:~/test$ 
"""
