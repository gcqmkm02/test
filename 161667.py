# -*- coding: utf-8 -*-

"""
# 참고문헌: https://kldp.org/node/161667

그런데 거기서 저는 ps -ef를 통해서 PID를 기준으로 잡고 내림차순 정렬하고 싶은데 
방법을 도저히 모르겠네요.
PID기준으로 UID , PID PPID 등을 출력하고 싶습니다.
"""

import subprocess
import sys

cpv = sys.hexversion
spv = 0x03070000

if cpv >= spv:
    p = subprocess.Popen(["ps", "-ef"], stdout=subprocess.PIPE, text=True)
else:
    print("Must be using Python 3.7 (or the above)")
    print("Python Version:", hex(cpv))
    sys.exit(1)

lines = p.stdout.readlines()

v0 = []
for x in lines:
    v0.append(x.split()[0])
UID = v0

v1 = []
for x in lines:
    v1.append(x.split()[1])
PID = v1

v2 = []
for x in lines:
    v2.append(x.split()[2])
PPID = v2

for x, y, z in zip(PID, UID, PPID): # 기준인 PID 를 가장 좌측으로...
    print(f'{x:8} {y:10} {z:8}')

print("\n--")
print("Python Version:", hex(sys.hexversion))

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 6월 3일

"""
# 출력 결과:

(bionic)soyeomul@localhost:~/test$ python3.7 161667.py
PID      UID        PPID    
1        root       0       
2        root       0       
3        root       2       
5        root       2       
7        root       2       
8        root       2       
9        root       2       
10       root       2       
11       root       2       
12       root       2       
13       root       2       
14       root       2       
16       root       2       
17       root       2       
18       root       2       
19       root       2       
21       root       2       
22       root       2       
23       root       2       
24       root       2       
26       root       2       
27       root       2       
28       root       2       
29       root       2       
30       root       2       
31       root       2       
32       root       2       
33       root       2       
34       root       2       
35       root       2       
36       root       2       
37       root       2       
38       root       2       
39       root       2       
41       root       2       
42       root       2       
65       root       2       
66       root       2       
67       root       2       
86       root       2       
87       root       2       
88       root       2       
89       root       2       
90       root       2       
95       root       2       
101      root       2       
123      root       2       
124      root       2       
125      root       2       
128      root       2       
129      root       2       
131      root       2       
132      root       2       
133      root       2       
134      root       2       
135      root       2       
136      root       2       
137      root       2       
138      root       2       
139      root       2       
140      root       2       
141      root       2       
142      root       2       
155      root       2       
156      root       2       
157      root       2       
158      root       2       
159      root       2       
160      root       2       
161      root       2       
162      root       2       
163      root       2       
164      root       2       
165      root       2       
166      root       2       
171      root       2       
172      root       2       
173      root       2       
174      root       2       
175      root       2       
176      root       2       
177      root       2       
178      root       2       
180      root       2       
207      root       1       
296      root       2       
297      root       2       
304      root       2       
305      root       2       
326      root       1       
337      root       2       
338      root       2       
339      root       2       
340      root       326     
342      root       326     
343      root       326     
344      root       326     
346      root       2       
349      root       2       
356      root       2       
357      root       2       
358      root       2       
359      root       2       
363      root       2       
364      root       2       
404      soyeomul   7506    
410      soyeomul   404     
471      root       2       
564      root       2       
579      202        1       
580      603        1       
593      root       2       
602      root       2       
607      root       2       
608      root       2       
625      201        1       
637      202        1       
765      219        1       
769      228        1       
956      229        1       
999      root       2       
1291     207        1       
1299     223        1       
1320     root       1       
1498     root       1       
1500     root       1498    
1501     20104      1498    
1502     202        1501    
1503     root       2       
1703     root       1       
1706     root       1       
1726     root       1       
1773     230        1       
1782     root       1       
1792     root       2       
1794     226        1       
1807     238        1       
1823     241        1       
1828     232        1       
1835     284        1       
1836     213        1       
1889     232        1828    
1894     600        1782    
1901     root       1835    
1911     238        1807    
1914     root       1       
1935     root       2       
1937     root       2       
1940     root       2       
1965     root       1       
2112     root       1       
2221     root       1       
2232     root       2       
2297     root       1       
2298     root       1       
2299     root       1       
2301     root       1       
2305     root       1       
2341     root       1       
2350     234        2299    
2387     root       2297    
2395     218        2341    
2399     root       2350    
2455     root       2       
2587     218        2301    
2588     218        2298    
2714     root       1       
2717     root       1       
2723     root       2717    
3186     root       2       
3227     root       2       
3301     root       2       
3761     root       2       
3781     root       2       
4078     root       2       
4195     root       1703    
4345     root       2       
4374     root       1706    
4378     root       1726    
4382     root       2221    
4386     root       2112    
4572     root       2       
4742     soyeomul   10698   
4760     soyeomul   10715   
4761     soyeomul   410     
4762     soyeomul   4761    
7506     root       1       
7549     root       1       
7553     soyeomul   7506    
7636     soyeomul   7553    
7639     soyeomul   7636    
7643     soyeomul   7636    
7646     soyeomul   7636    
7666     soyeomul   7553    
7671     soyeomul   7553    
7679     soyeomul   7666    
8045     soyeomul   7646    
8084     304        1       
8085     249        1       
8478     soyeomul   7646    
9449     soyeomul   7646    
9466     soyeomul   7553    
9560     soyeomul   9466    
9561     soyeomul   9560    
10286    root       9561    
10291    root       10286   
10565    message+   7506    
10571    root       7506    
10595    root       10291   
10602    soyeomul   10595   
10612    soyeomul   10602   
10689    soyeomul   10602   
10698    soyeomul   10689   
10699    soyeomul   10689   
10710    soyeomul   7506    
10715    soyeomul   10699   
10716    soyeomul   10699   
10717    soyeomul   10716   
10723    soyeomul   10715   
10724    soyeomul   10715   
10725    soyeomul   10715   
10726    soyeomul   10715   
10727    soyeomul   10715   
10728    soyeomul   10715   
10737    soyeomul   10689   
10754    soyeomul   7506    
10755    soyeomul   7506    
10790    soyeomul   7506    
10791    soyeomul   7506    
10800    soyeomul   10737   
10809    soyeomul   10737   
10812    soyeomul   7506    
10817    soyeomul   10812   
10819    soyeomul   7506    
10863    soyeomul   7506    
10868    soyeomul   7506    
10873    soyeomul   7506    
10889    soyeomul   7506    
10902    soyeomul   10737   
10909    root       7506    
10938    soyeomul   7506    
10955    soyeomul   10902   
10959    soyeomul   10955   
10961    soyeomul   7506    
10963    soyeomul   7506    
10974    soyeomul   7506    
10979    soyeomul   7506    
10988    soyeomul   7506    
10992    root       7506    
10997    root       7506    
11008    soyeomul   7506    
11013    geoclue    7506    
11018    root       7506    
11020    root       7506    
11025    soyeomul   7506    
11029    root       7506    
11037    soyeomul   7506    
11041    root       7506    
11065    soyeomul   7506    
11070    soyeomul   7506    
11075    soyeomul   7506    
11081    soyeomul   7506    
11087    root       7506    
11091    soyeomul   10737   
11093    soyeomul   10737   
11095    soyeomul   10737   
11098    soyeomul   10737   
11103    soyeomul   10737   
11108    soyeomul   10737   
11112    soyeomul   10737   
11114    soyeomul   10737   
11118    soyeomul   10737   
11131    soyeomul   10737   
11133    soyeomul   10737   
11134    soyeomul   10737   
11137    soyeomul   10737   
11139    soyeomul   10737   
11142    soyeomul   10737   
11144    soyeomul   10737   
11147    soyeomul   10737   
11157    soyeomul   7506    
11192    colord     7506    
11215    soyeomul   10737   
11223    soyeomul   7506    
11249    root       7506    
11266    soyeomul   11223   
11279    soyeomul   7506    
11281    soyeomul   10955   
11298    soyeomul   11279   
11332    soyeomul   10889   
11335    soyeomul   7506    
11576    soyeomul   10737   
11578    soyeomul   10737   
11604    root       7506    
12125    soyeomul   10737   
12270    soyeomul   10868   
12280    soyeomul   10868   
12306    soyeomul   10868   
19446    root       1       
19452    20106      19446   
21428    224        1501    
22570    root       2       
23716    soyeomul   10902   
25216    root       2       
25790    root       2       
25809    soyeomul   7646    
26305    soyeomul   7646    
27156    root       2       
27281    root       2       
27379    soyeomul   7646    
27390    soyeomul   7639    
28302    soyeomul   10715   
28932    soyeomul   10902   
29019    soyeomul   28932   
29069    soyeomul   28932   
30218    root       2       
31292    root       2       
31558    soyeomul   28932   
32007    root       2       
32613    root       2       

--
Python Version: 0x30701f0
(bionic)soyeomul@localhost:~/test$ 
"""
