/* -*- coding: utf-8 -*- */

#include <stdio.h>

int main()
{
	char str[4][7] = {
		"봄",
		"여름",
		"가을",
		"겨울",
	}; /* char 자료형으로 UTF-8 한글 처리 */

	printf("%s\n", str[0]);
	printf("%s\n", str[1]);
	printf("%s\n", str[2]);
	printf("%s\n", str[3]);

	return 0;
}

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막 갱신: 2020년 6월 3일
 */
