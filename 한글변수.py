#!/usr/bin/python3
# -*- coding: utf-8 -*-

# 파이썬3 부터는 변수명에다 한글을 쓸 수 있습니다.
# 그 한글들은 모두다 반드시 UTF-8 로 적어야 합니다.

숫자목록 = [1, 2, 3]
한글목록 = ['하나', '둘', '셋']
영문목록 = ['one', 'two', 'three']

for 갑,을,병 in zip(숫자목록,한글목록,영문목록):
    print(갑,'\t',을,'\t',병)

# 실행환경: 우분투 18.04, 파이썬 3.6.7
# 실행 결과는 다음과 같습니다:
'''
(bionic)soyeomul@localhost:~/work$ date
2019. 01. 26. (토) 00:11:44 KST
(bionic)soyeomul@localhost:~/work$ ./한글변수.py
1 	 하나 	 one
2 	 둘 	 two
3 	 셋 	 three
(bionic)soyeomul@localhost:~/work$ 
'''

# 참고문헌: zip() 함수
# https://docs.python.org/3.6/library/functions.html#zip

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 1월 26일
