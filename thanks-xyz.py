# -*- coding: utf-8 -*-

data = "^고맙습니다 _地平天成_ 감사합니다_^))//\n"
# 기반이 되는 문자열을 data 에 저장합니다.

def thanks():
    x = data[0:17]
    y = "_布德天下_"
    z = data[31:54]
    xyz = x+y+z
    return xyz
# thanks() 는 중간 문자열인 한자만을 변경하는 함수입니다.
# 나머진 그대로 갑니다.

print thanks()*3
# 3번 반복시켜 화면에 출력시킵니다.

# 실행 결과:
# ~/python_ruby $ python --version
# Python 2.7.3
# (precise)soyeomul@localhost:~/python_ruby$ python thanks-xyz.py
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
#
# (precise)soyeomul@localhost:~/python_ruby$ 
#
# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2017년 7월 24일
# 마지막 갱신: 2017년 7월 24일
