#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 기억의 저편에서 소의 되새김질처럼 밀려오는 임의의 문자열을 터미날에다
#  출력해주는 fortune 파이썬 코드이며 계속 갱신중입니다...;;;
# 또한 본 코드를 만드는데 동기부여해주신
#  렉스님(lexlove_)과 우분투 한국 대화방에 감사드립니다^^^
#
# 여기 기록된 모든 인용된 문자열과 파이썬 코드는 「Public Domain」 입니다.

def trandom(length):
    from time import time
    """
    ### 시간 기반 초간단 랜덤함수 ###

    random 모듈이 좀 무겁다는 생각에...
    저녁 소여물 주면서 계속 랜덤함수를 생각했었어요
    끊임없이 변하는것을 변하지 않는것으로 나누면 좋겠다싶었어요
    변하는건 시간, 변하지않는건 공간 그래서 이 함수가 만들어졌어요

    부처님오신날이라 그런지 
     이게 꼭 불교의 윤회(자연의 순환-반복하는 이치)처럼 느껴졌습니다
    --소여물, 2020년 4월 30일 (음력 4월 8일)
    """
    _constant = 13 # 시간을 좀 더 세분화시킴
    vtime = int(time() * _constant)
    
    return vtime % length

raw_data = """
그냥 재미로 만들었슈~
--토발즈
%
자유(free)는 소중한 가치입니다.
--스톨만
%
"우리는 문제를 숨기지 않을 것이다"
--데비안, ≪ 우리의 약속≫
%
^그대가 있어 행복합니다_^))//
--우분투
%
이놈 저놈 그놈 놈놈놈^^^
--익명의 그놈(GNOME) 애호가
%
<phuh> 왜 꼭 제가 이 채널을 체크하러 올때면
<phuh> 모두 다 다른데 가계시죠? ㅠㅠ
<phuh> 인생이 이런건가요
<bluedusk> 퇴근시간 이후에
<bluedusk> 오시니깐
<bluedusk> ...
--우분투 한국 대화방, 2014년 2월 11일
%
어차피 인간들의 모든 역사는 
승리한 자를 위해 꾸며지는 것
누군가는 지배하며 나머지는 따른다 
헤메는 쥐떼보다 정원에 메인 개가 나은 것
--The Power, <N.EX.T: Lazenca - A Space Rock Opera> - 1997년 발매
%
~아기상어 뚜루루루~~~
~아기상어 떼요~떼요~
--수아엄마와 수아의 대화中, 2018년 겨울 어느날 (서교동에서)
%
히트곡이 안될것 같아서 
 (난 상업적 음악을 안하겠다는 말을) 밑밥을 깔면서 얘기하는것은 
 예술가로서도 예뻐보이지 않으며 젊은이로써도 예뻐보이지 않더군요
--신해철의 고스트스테이션 방송中, 2011년 12월 2일
%
THANK YOU VERY SO MUCH!!!
--gmane.emacs.devel, 2020년 4월 27일
 (Emacs의 Wayland대응 개발 착수 소식을 접한 한 팬의 반응)
%
(남북 문제는) 깨지기 쉬운 유리그릇을 다루듯 
 조심스럽게 한걸음씩 나아가는 신중함이 필요하다
--문재인 대통령, 2019년 8월 19일 (청와대 수석-보좌관 회의에서)
%
"민주주의 최후의 보루는 깨어있는 시민의 조직된 힘입니다." 
"이것이 우리의 미래입니다"
--노무현 전 대통령, 2007년 6월 16일
%
민주주의는 목적에 있는 것이 아니라 수단과 방법에 있다
 무슨 말을 해도 3당 통합은 비민주적이고 반국민적이고 반역사적이다
--김대중 전 대통령, 1990년 2월 27일 (국회 평화민주당 대표연설에서)
%
문재인 정부의 법무부 장관이 된다면 
 서해맹산(誓海盟山)의 정신으로 
 공정한 법질서 확립, 검찰개혁, 법무부 혁신 등 소명을 완수하겠다
--조국 법무부 장관 후보자, 2019년 8월 9일 (인사청문회 준비 사무실에서)
%
역사의 수레바퀴를 뒤로 돌릴 것이냐, 앞으로 갈 것이냐
저는 이번 선거에서 _노무현_ 후보야말로 화합과 발전과 그리고 또 하나,
 제가 간절히 바라는 것이 있다면 이 땅의 사람들의 *삶의 가치를 회복*시켜줄
 유일한 후보라고 생각하기 때문에 이 자리에 섰습니다 
--신해철, 노무현 대통령 후보 지지연설中 (2002년 12월 7일) 
%
"하나의 작은 움직임이 큰 기적을"
--`Remember 0416' [배경: 2014년 4월 16일]
%
^고맙습니다 _布德天下_ 감사합니다_^))//
"""

data = raw_data.split("%")
data = [x.strip() for x in data if len(x) > 1]

idx = len(data)

print(data[trandom(idx)])

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 최초 작성일: 2020년 4월 30일
# 마지막 갱신: 2020년 5월 2일
