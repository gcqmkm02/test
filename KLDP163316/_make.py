#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 파일명: _make.py

# KLDP [163316]

# 원본 자료를 JSON 포맷으로 변환하기

# JSON 포맷 명세: RFC8259
# JSON 문법 검사: jsonlint.com

import re
import sys
import json

def _make(xyz):
    lst = xyz.split("\n")
    lst_tuple = []
    for k in lst:
        lst_tuple.append(tuple(k.split("=")))
    
    return dict(lst_tuple)

FPATH = sys.argv[1]

f = open(FPATH, "r")
data = f.read(); f.close()

s = re.split("\[p[0-9]?[0-9]\]", data)

"""불순물 제거후 필요한 정보들을 사전형으로 변환"""
s = [_make(x.strip()) for x in s if len(x) > 2]

KEY = ["[p{0}]".format(x['seq']) for x in s]
VALUE = [x for x in s]

dd = dict(zip(KEY, VALUE))

dj = json.dumps(dd, indent=4)

print(dj)

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 5월 19일
