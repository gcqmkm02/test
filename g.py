#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re

# 구글 계정 만들때 유효성 검사를 도와주는 코드입니다.

def f(xyz): # 계정 문자열 유효성 점검 함수
    """
    '.' 제외한 문자열의 길이는 8자부터 30자까지
    """
    if "." in xyz:
        chop = xyz.replace(".", "")
        if 8 <= len(chop) <= 30:
            pass
        else:
            print("string length not good ;;;")
            sys.exit(1)
    elif 8 <= len(xyz) <= 30:
        pass
    else:
        print("string length not good ;;;")
        sys.exit(1)

    """
    입력받은 문자열을 분석하여 유효성 유무 연산
    """
    p = re.compile("([a-zA-Z0-9]([a-zA-Z0-9]*[.]?[a-zA-Z0-9]*)+)")
    m = p.search(xyz)
    """연속적으로 '.' 허용안되며, 문자열 맨끝에도 '.' 허용안됨"""
    if ".." not in xyz and xyz[-1] != "." and len(xyz) == len(m.group(1)):
        print("OK, yours is <{}@gmail.com> ;;;".format(xyz))
        print(len(xyz), len(m.group(1)), m.group(1)) # 디버깅 위하야...
    else:
        print("sorry, you wrong string ;;;")
        print(len(xyz), len(m.group(1)), m.group(1)) # 디버깅 위하야...
        
f(sys.argv[1])

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 25일