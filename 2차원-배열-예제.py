#!/usr/bin/env python3
# -*- coding: utf-8 -*-

a = [[0 for rows in range(3)]for cols in range(4)]

a[1][2] = "6월"
a[3][0] = "10월"

print(a)

"""
(bionic)soyeomul@localhost:~/test$ ./2차원-배열-예제.py
[[0, 0, 0], [0, 0, '6월'], [0, 0, 0], ['10월', 0, 0]]
(bionic)soyeomul@localhost:~/test$ 
"""
