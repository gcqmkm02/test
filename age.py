#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

# 송아지 출생일자를 입력받아서 나이(개월령)를 출력해주는 코드입니다.

import datetime
import re

exp = re.compile("^(\d\d\d\d).*(\d\d).*(\d\d)$")
rs = raw_input("출생일자를 입력하세요 (예: 2017-08-17): ")

rsd = exp.search(rs)

ix = int(rsd.group(1))
iy = int(rsd.group(2))
iz = int(rsd.group(3))

x = ix

if str(iy)[0] == '0':
    y = iy[1]
else:
    y = iy

if str(iz)[0] == '0':
    z = iz[1]
else:
    z = iz

tday = datetime.date.today()
bday = datetime.date(x,y,z) # x, y, z 는 정수형 타입
timedelta = tday - bday

std = str(timedelta).split()[0]

age = int(std)

if age*12%365 < 15:
    mage = age*12/365
else:
    mage = age*12/365 + 1 # age in months (개월령)

print str(mage) + "개월령", "(" + rs + ")"

# 편집: Emacs 23.3 (Ubuntu 12.04)
# 마지막 갱신: 2018년 1월 28일
