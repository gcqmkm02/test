#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 우분투 한국 대화방 (#ubuntu-ko) 원격 grep 도구
# (1) 하루치 로그 검색 가능
#     ===> "+" 열쇠말 추가시에 그 다음날도 이어서 추가 검색함
# (2) 한달치 로그 검색 가능
#     ===> "+" 열쇠말 추가시에 그 다음달도 이어서 추가 검색함
# (3) 일년치 로그 검색 가능
# (4) 전체 로그 주소 출력 가능 (하루치만)
# (5) 결과행의 날짜 태그 적용
# (6) 결과행에서 검색어 쉽게 식별할 수 있도록 밑줄 표시
# (7) UTF-8 에러나는 로그도 검색할 수 있도록 코드 보완
# (8) 커서 순환/반복: 지루함 방지용

# 실행환경:
# 파이썬 3 / curl 필요함
# 가급적 리눅스/유닉스 시스템에서 실행할것

# 도움주신분: 서니님(Seony)
# 20190622 -- 순환문에 관한 화두를 던져주셨음 
# 20190530 -- 커서 순환/반복에 관한 조언을 주셨음

import subprocess
import re
from datetime import datetime, timedelta
import sys, time, threading

HURL = "https://forum.ubuntu-kr.org/viewtopic.php?f=6&t=30131"

def make_durl(xyz): # 하루치 로그
    year = xyz[0:4]
    month = xyz[4:6]
    day = xyz[6:8]

    FURL = []
    FURL.append("https://irclogs.ubuntu.com/{0}/{1}/{2}/%23ubuntu-ko.txt"\
        .format(year,month,day))

    return FURL

def make_murl(xyz): # 한달치 로그
    year = xyz[0:4]
    month = xyz[4:6]

    ds = list(range(1,32))

    rs = []
    for x in ds:
        rs.append(str(x).zfill(2))

    FURL = []
    for day in rs:
        FURL.append("https://irclogs.ubuntu.com/{0}/{1}/{2}/%23ubuntu-ko.txt"\
                    .format(year,month,day))

    return FURL

def make_yurl(xyz): # 일년치 로그
    year = sys.argv[2]

    m01 = ["01"] * 31
    m02 = ["02"] * 31
    m03 = ["03"] * 31
    m04 = ["04"] * 31
    m05 = ["05"] * 31
    m06 = ["06"] * 31
    m07 = ["07"] * 31
    m08 = ["08"] * 31
    m09 = ["09"] * 31
    m10 = ["10"] * 31
    m11 = ["11"] * 31
    m12 = ["12"] * 31

    month_list = \
        m01 +\
        m02 +\
        m03 +\
        m04 +\
        m05 +\
        m06 +\
        m07 +\
        m08 +\
        m09 +\
        m10 +\
        m11 +\
        m12

    ds = list(range(1,32))
    rs = []
    for x in ds:
        rs.append(str(x).zfill(2))
    day = rs * 12

    FURL = []
    index = 0
    for month in month_list:
        FURL.append("https://irclogs.ubuntu.com/{0}/{1}/{2}/%23ubuntu-ko.txt"\
                    .format(year,month,day[index]))
        index += 1

    return FURL

def make_xdurl(xyz): # 하루치 추가 로그: "+" 열쇠말 분석
    x = xyz[0:4]
    y = xyz[4:6]
    z = xyz[6:8]

    input_date = datetime(int(x),int(y),int(z))
    next_date = input_date + timedelta(days=1)
    next_date_str = str(next_date)
    
    the_date = next_date_str[0:10].split("-")

    year = the_date[0]
    month = the_date[1]
    day = the_date[2]

    FURL = []
    FURL.append("https://irclogs.ubuntu.com/{0}/{1}/{2}/%23ubuntu-ko.txt"\
                .format(year,month,day))

    return FURL

def make_xmurl(xyz): # 한달치 추가 로그: "+" 열쇠말 분석
    year_str = xyz[0:4]
    month_str = xyz[4:6]
    
    if month_str == "12":
        year = str(int(year_str)+1)
        month = "01"
    else:
        year = year_str
        month_str = str(int(month_str)+1)
        month = month_str.zfill(2)

    ds = list(range(1,32))
    rs = []
    for x in ds:
        rs.append(str(x).zfill(2))

    FURL = []
    for day in rs:
        FURL.append("https://irclogs.ubuntu.com/{0}/{1}/{2}/%23ubuntu-ko.txt"\
                    .format(year,month,day))

    return FURL

if len(sys.argv) < 2 or sys.argv[1] == "--help":
    print("""\
사용법: %s 찾을문자열 찾을년월일(또는 찾을년월 또는 찾을년)

고급사용법: %s 찾을문자열 찾을년월+
고급사용법: %s 찾을문자열 찾을년월일+

사용 예제: %s
""" % (sys.argv[0],sys.argv[0],sys.argv[0],HURL))
    sys.exit(1)
elif len(sys.argv) == 2: # 찾을년(월일) 생략할 경우 기본값으로 할당될 로그 주소
    FURL = ["https://irclogs.ubuntu.com/2019/01/26/%23ubuntu-ko.txt"]
elif len(sys.argv[2]) == 4: # 일년치 로그
    FURL = make_yurl(sys.argv[2])
elif len(sys.argv[2]) == 6: # 한달치 로그
    FURL = make_murl(sys.argv[2])
elif sys.argv[2][6] == "+": # 고급사용법 적용 (한달치 추가 로그)
    FURL = make_murl(sys.argv[2]) + make_xmurl(sys.argv[2])
elif len(sys.argv[2]) == 8:
    FURL = make_durl(sys.argv[2])
elif sys.argv[2][8] == "+": # 고급사용법 적용 (하루치 추가 로그)
    FURL = make_durl(sys.argv[2]) + make_xdurl(sys.argv[2])

if sys.argv[1] == "--full" and len(sys.argv) ==2:
    print("사용법: %s --full 년월일" % (sys.argv[0]))
    sys.exit(1)
elif sys.argv[1] == "--full" and len(sys.argv[2]) == 8: # 하루치 전체 로그 URL을 웹브라우저로 보길 원할때...
    print((make_durl(sys.argv[2])[0]).replace("txt","html"))
    sys.exit(0)

class colors:
    """
    POSIX 시스템 터미널에서 글자에 색깔 적용하기: 
    https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-terminal-in-python
    """
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    
def get_grep(url): # grep 내부 동작 방식
    """ -f 옵션이 대략 1초 정도 단축시킴 [20190901] """
    p = subprocess.Popen("curl -f -s {}".format(url), \
                         stdout=subprocess.PIPE, shell=True)
    output = p.communicate()[0]

    data_full = output.decode("utf-8", "replace") # UTF-8 문자열로 변환 
    data = data_full.splitlines()
    
    exp = re.compile("com/(.+)/(.+)/(.+)/")
    chop1 = exp.search(url).group(1)
    chop2 = exp.search(url).group(2)
    chop3 = exp.search(url).group(3)
    fdate = chop1 + chop2 + chop3 # 검색된 문장의 날짜 태그

    for line in data:
        if sys.argv[1].lower() in line.lower(): # 대소문자 구분 없이 검색가능
            """
            \b ===> 커서 순환/반복 대응
            {0}{1}{2} ===> 검색어 눈에 띄게끔 밑줄 표시 (영문 대문자는 경우에 따라 다를수도 있음)
            """
            print("\b", fdate, \
                  line.replace(sys.argv[1],"{0}{1}{2}"\
                               .format(colors.UNDERLINE,sys.argv[1],colors.ENDC)))

start_time = datetime.now() # 시간 측정 시작.
print("... 검색중 ... 커피한잔과 함께,,, 천천히 기다려주세요 ...^^;;;")

# 지루함 방지용: 커서 순환/반복 ...
"""
Spinner taken from:
https://stackoverflow.com/questions/48854567/python-asynchronous-progress-spinner
"""
def spin_cursor():
    while True:
        for cursor in "-\\|/":
            sys.stdout.write(cursor)
            sys.stdout.flush()
            time.sleep(0.3) # 속도 조절
            sys.stdout.write("\b")
            if done:
                return

# start the spinner in a separate thread
done = False
spin_thread = threading.Thread(target=spin_cursor)
spin_thread.start()

# 커서 순환/반복할 동안 작업할 목록 아래에 기록:
for url in FURL:
    get_grep(url) # 찾을문자열 검색후 결과행 출력

# tell the spinner to stop, and wait for it to do so;
# this will clear the last cursor before the program moves on
done = True
spin_thread.join()

# 커서 순환/반복은 끝났지만, 다음 할일을 아래에다 기록:
time_elapsed = datetime.now() - start_time # 시간 측정 끝.
print("# 검색시간(hh:mm:ss.ms): %s" % (time_elapsed))

# 아래는 디버깅을 위하야 추가정보로 남겨둠
print("# FURL:", type(FURL)) # FURL 은 *반드시* 리스트(배열) 형식이어야 함

sys.stdout.write("^고맙습니다 _布德天下_ 감사합니다_^))//\n")

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 9월 1일
