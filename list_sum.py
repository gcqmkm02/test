#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

# 각 리스트의 합을 구하는 코드입니다.

x = 2**1000
y = list(str(x))
z = sum(int(i) for i in y)

print z # 결과값: 1366
