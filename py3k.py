# -*- coding: utf-8 -*-

# 실전 파이썬 판번호 식별 방법 
# 출처: <https://github.com/ibus/ibus/blob/master/engine/iso639converter.py>

import sys

PY3K = sys.hexversion >= 0x03000000  

if PY3K:
    print("예, 전 파이썬 3 입니다.")
    print("Python Version:", sys.version_info[0:3], hex(sys.hexversion))
else:
    print("미안해요 전 파이썬 3 이 아니어유 ㅜㅜㅜ")
    print("Python Version:", sys.version_info[0:3], hex(sys.hexversion))

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 6월 17일
