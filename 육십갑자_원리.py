#!/usr/bin/python3
# -*- coding: utf-8 -*-

x = ['0', '1', '0', '1', '0', '1']
y = ['A', 'B', 'C', 'A', 'B', 'C']
# 육십갑자의 천간과 지지의 조합 방식 간단 원리

index = 0

for i in x:
    print("%s%s" %(i, y[index]))
    index += 1
# 우분투 대화방의 써니님께서 조언해주심.

#실행 결과는 아래에:
'''
(bionic)soyeomul@localhost:~/work$ ./3.py
0A
1B
0C
1A
0B
1C
(bionic)soyeomul@localhost:~/work$ 

'''

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 편집: 2019년 1월 26일
