#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# sys.argv 음메~~~! 

import sys

def f(x):
    return "음메~~~!"

if len(sys.argv) < 2 or str(sys.argv[1]) != "깐돌깐돌":
    print("사용법: %s 깐돌깐돌" % sys.argv[0])
    sys.exit(1)
else:
    print("{}".format(f(sys.argv[1])))

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 6월 20일

"""
테스트 결과: 우분투 18.04, 파이썬 3.6.7

(bionic)soyeomul@localhost:~/test$ ./9.py
사용법: ./9.py 깐돌깐돌
(bionic)soyeomul@localhost:~/test$ ./9.py --help
사용법: ./9.py 깐돌깐돌
(bionic)soyeomul@localhost:~/test$ ./9.py 깐돌깐돌
음메~~~!
(bionic)soyeomul@localhost:~/test$ 
"""
