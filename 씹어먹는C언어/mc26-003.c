/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>

/* 
 * 씹어먹는 C언어: 1부터 n까지의 합 
 * https://modoocode.com/26
 */

/*** ### 산법: 파이썬3 ###
import sys

def f(n):
    _sum = 0 
    for i in range(int(n)+1):
        _sum = _sum + i
	
    return _sum

입력 = sys.argv[1]

print(f(입력))
***/

int my_sum(int n)
{
	int i;
	int _sum = 0;
	for (i = 0; i < n+1; i++) {
		_sum = _sum + i;
	}

	return _sum;
}

int main(int argc, char **argv)
{
	int j = atoi(argv[1]);

	printf("입력값: %d, 출력값: %d\n", j, my_sum(j));

	return 0;
}

/*** ### 실행결과 ###
(bionic)soyeomul@localhost:~/111$ ./a.out 1
입력값: 1, 출력값: 1
(bionic)soyeomul@localhost:~/111$ ./a.out 2
입력값: 2, 출력값: 3
(bionic)soyeomul@localhost:~/111$ ./a.out 3
입력값: 3, 출력값: 6
(bionic)soyeomul@localhost:~/111$ ./a.out 10
입력값: 10, 출력값: 55
(bionic)soyeomul@localhost:~/111$ ./a.out 100
입력값: 100, 출력값: 5050
(bionic)soyeomul@localhost:~/111$ gcc --version
gcc (Ubuntu/Linaro 7.5.0-3ubuntu1~18.04) 7.5.0
Copyright (C) 2017 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

(bionic)soyeomul@localhost:~/111$ 
***/

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막 갱신: 2020년 6월 12일
 */
