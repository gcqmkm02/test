#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 파일명: make_json.py

# 역할: fortune 데이타 파일을 JSON 포맷으로 변환합니다

# JSON 포맷은 범용적인 인터넷 데이타 교환 포맷입니다
# JSON 포맷 명세: https://tools.ietf.org/html/rfc8259 (INTERNET STANDARD)
# 파이썬3 JSON: https://docs.python.org/3.6/library/json.html

"""
# 사용법: (쉘에서 실행합니다)
$ ./make_json.py example.moon > ~/.문장/example.json
$ ./make_json.py /usr/share/games/fortunes/startrek > /tmp/startrek.json
"""

# 실험환경: 우분투 18.04 LTS, 파이썬 3.6.9

# 모든 파이썬 코드는 「Public Domain」 입니다 ^^^

import json
import sys

def make_json(datafile):
    f = open(datafile, "r")
    data = f.read().split("%"); f.close()
    
    data = [x.strip() for x in data if len(x) > 2]
    data = [tuple(x) for x in enumerate(data)]

    dd = dict(data)

    dj = json.dumps(dd, indent=4)

    return dj

datafile = sys.argv[1]

print(make_json(datafile))

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 최초 작성일: 2020년 5월 7일
# 마지막 갱신: 2020년 5월 9일
