#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

import subprocess
import sys

def make_url(xyz):
    x = xyz[0:4]
    y = xyz[4:6]
    z = xyz[6:8]

    FURL = "https://irclogs.ubuntu.com/{0}/{1}/{2}/%23ubuntu-ko.txt".format(x,y,z)

    return FURL

if len(sys.argv) < 2:
    print("사용법: {} 찾을문자열 날짜".format(sys.argv[0]))
    print("$ {} 안녕하세요 20190126".format(sys.argv[0]))
    sys.exit(1)
elif 2 <= len(sys.argv) < 3:
    FURL = "https://irclogs.ubuntu.com/2019/01/26/%23ubuntu-ko.txt"
else:
    FURL = make_url(sys.argv[2])
    
p = subprocess.Popen("curl -s {}".format(FURL), \
                    stdout=subprocess.PIPE, text=True, shell=True)

data = p.stdout.readlines()

for line in data:
    if sys.argv[1].lower() in line.lower():
        try:
            if len(sys.argv) < 3:
                print(line, end="")
            else:
                print("{}".format(sys.argv[2]), line, end="")
        except:
            pass

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 6월 21일
