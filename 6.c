/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int lp[9999] = {2,};
int llp = 1; /* 소수 누적 갯수 */

int prime(int n)
{
	char F[8] = {'\'', 'F', 'a', 'l', 's' , 'e', '\'', '\0',};
	char T[7] = {'\'', 'T', 'r', 'u', 'e', '\'', '\0',};
	
	if (n == 2) {
		printf("(%d, %s)\n", n, T);
		
		return 0;
	}
	
	int i;
	
	for (i = 0; i < llp; i++) {
		if (n != lp[i] && n % lp[i] == 0) {
			printf("(%d, %s)\n", n, F);
			
			return 0;
		}
	}
	
	lp[llp] = n;
	llp = llp + 1;
	
	printf("(%d, %s)\n", n, T);
	/* printf("누적갯수: %d\n", llp); */

	return 0;
}

int main(int argc, char **argv)
{
	int i;
	int j = atoi(argv[1]);
       
	for (i = 2; i < j+1; i++) {
		prime(i);
	}

	return 0;
}

/*
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막 갱신: 2020년 4월 4일
 */

/* EOF */
