# -*- coding: utf-8 -*-

import math
# 팩토리얼 함수를 위하야 ...
# https://docs.python.org/2/library/math.html#math.factorial

data = "^고맙습니다 _地平天成_ 감사합니다_^))//\n"
# 기반이 되는 문자열을 data 에 저장합니다.

def thanks():
    x = data[0:17]
    y = "_布德天下_"
    z = data[31:54]
    xyz = x+y+z
    return xyz
# thanks() 는 중간 문자열인 한자만을 변경하는 함수입니다.
# 나머진 그대로 갑니다.

print thanks()*math.factorial(3)
# 3! 만큼 감사함을 화면에 뿌려주세요~

# 실행 결과:
# ~/python_ruby $ python --version
# Python 2.7.3
# (precise)soyeomul@localhost:~/python_ruby$ python thanks-factorial.py
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
#
# (precise)soyeomul@localhost:~/python_ruby$ 
#
# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2017년 7월 25일
# 마지막 갱신: 2017년 7월 25일
