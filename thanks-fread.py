# -*- coding: utf-8 -*-

# 기계 내부에 존재하는 파일을 읽는 코드입니다.
# 참고문헌: https://wikidocs.net/26

f = open("/home/soyeomul/.signature", "r")
content = f.read()

print content
f.close()

# 실행 결과:
# (precise)soyeomul@localhost:~/python_ruby$ python --version
# Python 2.7.3
# (precise)soyeomul@localhost:~/python_ruby$ python thanks-fread.py
# ^고맙습니다 _地平天成_ 감사합니다_^))//
# (precise)soyeomul@localhost:~/python_ruby$ 
#
# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2017년 7월 24일
# 마지막 갱신: 2017년 7월 25일
