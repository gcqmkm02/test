#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 시스템 계정 출력
# 참고문헌: https://kldp.org/node/161562

f = open("/etc/passwd", "r")
lines = f.readlines()

v0 = []
for x in lines:
    v0.append(x.split(":")[0])
account_id = v0

v6 = []
for x in lines:
    v6.append(x.split(":")[6])
account_shell = v6

for v1,v2 in zip(account_id,account_shell):
    print(f'{v1:19} ===> {v2.strip():19}')

f.close()

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 5월 10일

"""
테스트 결과: 우분투 18.04, 파이썬 3.6.7

(bionic)soyeomul@localhost:~/test$ ./2.py
root                ===> /bin/bash          
daemon              ===> /usr/sbin/nologin  
bin                 ===> /usr/sbin/nologin  
sys                 ===> /usr/sbin/nologin  
sync                ===> /bin/sync          
games               ===> /usr/sbin/nologin  
man                 ===> /usr/sbin/nologin  
lp                  ===> /usr/sbin/nologin  
mail                ===> /usr/sbin/nologin  
news                ===> /usr/sbin/nologin  
uucp                ===> /usr/sbin/nologin  
proxy               ===> /usr/sbin/nologin  
www-data            ===> /usr/sbin/nologin  
backup              ===> /usr/sbin/nologin  
list                ===> /usr/sbin/nologin  
irc                 ===> /usr/sbin/nologin  
gnats               ===> /usr/sbin/nologin  
nobody              ===> /usr/sbin/nologin  
systemd-timesync    ===> /bin/false         
systemd-network     ===> /bin/false         
systemd-resolve     ===> /bin/false         
syslog              ===> /bin/false         
_apt                ===> /bin/false         
messagebus          ===> /bin/false         
colord              ===> /bin/false         
pulse               ===> /bin/false         
soyeomul            ===> /bin/bash          
avahi               ===> /usr/sbin/nologin  
saned               ===> /usr/sbin/nologin  
geoclue             ===> /usr/sbin/nologin  
cups-pk-helper      ===> /usr/sbin/nologin  
kernoops            ===> /usr/sbin/nologin  
avahi-autoipd       ===> /usr/sbin/nologin  
gdm                 ===> /bin/false         
whoopsie            ===> /bin/false         
dnsmasq             ===> /usr/sbin/nologin  
speech-dispatcher   ===> /bin/false         
gnome-initial-setup ===> /bin/false         
hplip               ===> /bin/false         
postfix             ===> /usr/sbin/nologin  
(bionic)soyeomul@localhost:~/test$ 
"""
