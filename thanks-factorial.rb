# -*- coding: utf-8 -*-

$data = "^고맙습니다 _地平天成_ 감사합니다_^))//\n"
# "$" 표시는 전역 변수 선언을 의미합니다. 
# 전역 변수를 선언치 않으면 thanks() 함수에서
# data 를 찾을 수 없다고 에러를 내뱉더이다..

def thanks()
  x = $data[0...17]
  y = "_布德天下_"
  z = $data[31...54]
  xyz = x+y+z
  return xyz
end
# 그라설라무네 중간의 한자를 바꾸는 함수입니다..
# 리스트 "[]" 내에서 "..." 의 용법은 파이썬의 ":" 와 동일합니다.

# 팩토리얼: 3! = 3x2x1 = 6
# 참고문헌: https://stackoverflow.com/questions/2434503/ruby-factorial-function
def f(n)
  (1..n).inject(:*) || 1
end
# inject 는 긴 코드를 짧게 줄여주는 메쏘드(Method) 같은데 아직 이해 못했어여ㅠㅠ
# 본 팩토리얼 함수로 돌려본 100000! 결과는 대략 십의 사십오만 거듭제곱보다도 훨씬 
# 더 큰 수가 출력되었어요. 결과값은 아래에 링크로 남깁니다:
# https://github.com/soyeomul/stuff/blob/master/100000-factorial.txt.ruby

f(3).times { print thanks() }
# 3! 만큼 감사함을 반복시켜서 화면에 뿌려주세요~

# 실행 결과:
# ~/python_ruby $ ruby --version
# ruby 1.8.7 (2011-06-30 patchlevel 352) [i686-linux]
# (precise)soyeomul@localhost:~/python_ruby$ ruby thanks-factorial.rb
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# (precise)soyeomul@localhost:~/python_ruby$ 
#
# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2017년 7월 25일
# 마지막 갱신: 2017년 7월 26일
