# -*- coding: utf-8 -*-

$data = "^고맙습니다 _地平天成_ 감사합니다_^))//\n"
# "$" 표시는 전역 변수 선언을 의미합니다. 
# 전역 변수를 선언치 않으면 thanks() 함수에서
# data 를 찾을 수 없다고 에러를 내뱉더이다..

def thanks()
  x = $data[0...17]
  y = "_布德天下_"
  z = $data[31...54]
  xyz = x+y+z
  return xyz
end
# 그라설라무네 중간의 한자를 바꾸는 함수입니다..
# 리스트 "[]" 내에서 "..." 의 용법은 파이썬의 ":" 와 동일합니다.

open("/tmp/thanks", "w") do |f| 
  f << thanks()*3
end
# 3줄의 감사함을 /tmp/thanks 에 담아봅니다~
# 참고문헌: (루비 파일 쓰기 방법 예제들)
# https://alvinalexander.com/blog/post/ruby/how-write-text-to-file-ruby-example

# 루비 판번호: ruby 1.8.7 (2011-06-30 patchlevel 352) [i686-linux]
# 실행 결과: $ cat /tmp/thanks
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//
# ^고맙습니다 _布德天下_ 감사합니다_^))//

# 편집: Emacs 23.3 (Ubuntu 12.04)
# 최초 작성일: 2017년 7월 25일
# 마지막 갱신: 2017년 7월 25일
