# -*- coding: utf-8 -*-

# 참조문헌: https://kldp.org/node/161566

# 우분투 18.04 에서 파이썬 3.7 까는법
# sudo apt-get update
# sudo apt-get install python3.7
# 실행파일위치: /usr/bin/python3.7

import subprocess
import sys

cpv = sys.hexversion # 현재 파이썬 판번호
spv = 0x03070000 # 기준 파이썬 판번호 "3.7"

if cpv >= spv:    
    p = subprocess.Popen(["ps", "-ef"], stdout=subprocess.PIPE, text=True)
else:
    print("Must be using Python 3.7 (or the above)")
    print("Python Version:", hex(cpv))
    sys.exit(1)
    
for proc in p.stdout:
    if '?' not in proc:
        print(proc.strip())

print("{}--".format("\n"))
print("Python Version:", hex(cpv))

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 5월 29일

"""
# 테스트 결과:

(bionic)soyeomul@localhost:~/test$ python 333.py
Must be using Python 3.7 (or the above)
('Python Version:', '0x2070fc1')
(bionic)soyeomul@localhost:~/test$ python3 333.py
Must be using Python 3.7 (or the above)
Python Version: 0x30607f0
(bionic)soyeomul@localhost:~/test$ python3.7 333.py
UID        PID  PPID  C STIME TTY          TIME CMD
root       370   334  0  5월11 pts/1  00:00:00 /sbin/agetty - 9600 xterm
root       374   334  0  5월11 pts/2  00:00:00 /sbin/agetty - 9600 xterm
root       379   334  0  5월11 pts/3  00:00:00 /sbin/agetty - 9600 xterm
soyeomul 13048  1346  0  5월27 pts/0  00:00:00 /bin/bash /usr/bin/crosh
soyeomul 13144 13048  0  5월27 pts/0  00:00:00 /bin/bash /usr/bin/crosh
soyeomul 13145 13144  0  5월27 pts/0  00:00:00 /bin/bash -l
root     25691 13145  0 09:23 pts/0    00:00:00 sudo LD_LIBRARY_PATH=/usr/local/lib startgnome
root     25696 25691  0 09:23 pts/0    00:00:00 sh -e /usr/local/bin/enter-chroot -t gnome  exec startgnome
root     26006 25696  0 09:24 pts/0    00:00:00 su -s /bin/sh -c export SHELL='/bin/bash';'exec' 'startgnome'  - soyeomul
soyeomul 28290 28284  0 09:30 pts/4    00:00:00 bash
soyeomul 30599 28290  0 09:45 pts/4    00:00:00 python3.7 333.py
soyeomul 30600 30599  0 09:45 pts/4    00:00:00 ps -ef

--
Python Version: 0x30701f0
(bionic)soyeomul@localhost:~/test$ 
"""
