# -*- coding: utf-8 -*-

import re

목록 = ["나11", "가3", "나2", "가12"]
가 = [x for x in 목록 if "가" in x]
나 = [x for x in 목록 if "나" in x]

def 새정렬(xyz):
    temp = []
    for k in xyz:
        p = re.search("([0-9]?[0-9])", k).group(1)
        pp = int(p) # 이것을 위하야 코드가 길어졌음
        temp.append(pp)
    new_xyz = tuple(zip(xyz, temp))
    새정렬 = sorted(new_xyz, key=lambda x: x[1])
    새정렬 = [x[0] for x in 새정렬]

    return 새정렬
    
가 = 새정렬(가)
나 = 새정렬(나)

새목록 = 가 + 나

print(새목록) # ['가3', '가12', '나2', '나11']

# 편집: VIM (Ubuntu 18.04)
# 마지막 갱신: 2020년 6월 17일
