/* -*- coding: utf-8 -*- */

#include <stdio.h>
#include <stdlib.h>

/* 더하기/빼기 연산 예제 */

int cal(int a, int b, char *c)
{
	if (*c == '+') {
		return a + b;
	}
	
	if (*c == '-') {
		return a - b;
	}
	
	return 0;
}

int main(int argc, char **argv)
{
	int i = atoi(argv[1]);
        int j = atoi(argv[2]);

	char *k = argv[3];
	
	printf("결과: %d\n", cal(i, j, k));

	return 0;
}

/* 
 * 편집: GNU Emacs 26.3 (Ubuntu 18.04)
 * 마지막 갱신: 2020년 4월 7일
 */

/* EOF */
