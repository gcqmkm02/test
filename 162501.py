# -*- coding: utf-8 -*-

# KLDP 162501

# [3] 엔 이름과 논문제목 사이에 ":" 대신 "." 이 있어서 전체적으로 규칙성을 부여하기 힘드므로
# 원본자료를 "." 대신 ":" 으로 수정을 했습니다.
kdata = """\
[1] B. Aditya, G. Bhalotia, S. Chakrabarti, A. Hulgeri, C. Nakhe, P. Parag, and S. Sudarshan. Banks: browsing and keyword searching in relational databases. In Proc. VLDB Conf., pages 1083–1086. VLDB Endowment, 2002.
 
[2] S. Agrawal, S. Chaudhuri, and G. Das. Dbxplorer: A system for keyword-based search over relational databases. In Proc. ICDE Conf., page 5, Washington, DC, USA, 2002. IEEE Computer Society.
 
[3] S. Basu Roy, H. Wang, G. Das, U. Nambiar, and M. Mohania: Minimum-effort driven dynamic faceted search in structured databases. In Proc. CIKM, pages 13–22, 2008.
"""
 
import re
 
klines = kdata.split("\n")

kname = []
ksubject = []
kyear = []
 
def kname_search(s):
    p = re.search("\].+(?=:)", s)
    pp = p.group(0)[2:]
    kname.append(pp)
 
def ksubject_search(s):
    p = re.search("(?<=: ).+[a-z][.]", s)
    pp = p.group(0)
    ppp = pp.split(".")[0]
    ksubject.append(ppp)
 
def kyear_search(i):
    p = re.search("19[0-9][0-9]|20[0-9][0-9]", i)
    kyear.append(p.group(0))
 
for line in klines:
    if len(line) > 1:
        kname_search(line)
 
for line in klines:
    if len(line) > 1:
        ksubject_search(line)
 
for line in klines:
    if len(line) > 1:
        kyear_search(line)
 
for x, y, z in zip(kyear, ksubject, kname):
    print(x, "\t", y, "\t", z)

# 편집: Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2019년 12월 8일

"""
출력실험: 우분투 18.04/ 파이썬 3.7.5

(bionic)soyeomul@localhost:~/kldp$ python3.7 --version
Python 3.7.5
(bionic)soyeomul@localhost:~/kldp$ python3.7 1_new.py
2002 	 browsing and keyword searching in relational databases 	 B. Aditya, G. Bhalotia, S. Chakrabarti, A. Hulgeri, C. Nakhe, P. Parag, and S. Sudarshan. Banks
2002 	 A system for keyword-based search over relational databases 	 S. Agrawal, S. Chaudhuri, and G. Das. Dbxplorer
2008 	 Minimum-effort driven dynamic faceted search in structured databases 	 S. Basu Roy, H. Wang, G. Das, U. Nambiar, and M. Mohania
"""
