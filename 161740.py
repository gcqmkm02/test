# -*- coding: utf-8 -*-

# https://kldp.org/node/161740

FPATH = "data.txt"
f = open(FPATH, "r")
data = f.read()
data_new = data.replace("246 DirectoryIndex index.html", "246 DirectoryIndex index.html index.htm index.php")

with open("{}.orig".format(FPATH), "w") as of:
    of.write(data)

with open(FPATH, "w") as nf:
    nf.write(data_new)
    
f.close()

"""
data.txt:

'''
245
246 DirectoryIndex index.html
247
''' 
"""
