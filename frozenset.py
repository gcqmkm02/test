# -*- coding: utf-8 -*-

# https://chromium.googlesource.com/chromiumos/chromite/+/refs/heads/master/config/chromeos_config_boards.py

s = frozenset([
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'a',
])

print(type(s))

print(s)

for k in s:
    print(k)
    
"""
출력결과 'a' 는 하나만 출력됨.
중복 실행을 방지하기위한 조치.
frozenset 의 목적.
"""

# 편집: Emacs 26.2 (Ubuntu 18.04)
# 마지막 갱신: 2019년 7월 7일
